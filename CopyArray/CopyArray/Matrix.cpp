#include "Matrix.h"
/**
 * Конструктор по умолчанию
 * по умолчанию размер матрицы = 2
 * по умолчанию занчение матрицы = 0
 */
Matrix::Matrix()
{
    SizeN = 2;
    SizeM = 2;

    Massive = new int*[SizeN];// выделить место в памяти для матрицы

    for (int i = 0; i < SizeN; i++)
    {
        Massive[i] = new int[SizeM];
        for (int j = 0; j < SizeM; j++)
        {
            Massive[i][j] = 0;// заполняем 0
        }
    }
}

/**
* Конструктор с параметрами
* @param N - Кол-во строк
* @param M - Кол-во колонок
* Массив заполняется случайным значением при помощи функции @func rand() 
*/
Matrix::Matrix(int N, int M)
{
    SizeN = N;// количество строк
    SizeM = M;// количество колонок

    Massive = new int*[SizeN];// выделить место в памяти для матрицы

    for (int i = 0; i < SizeN; i++)
    {
        Massive[i] = new int[SizeM];
        for (int j = 0; j < SizeM; j++)
        {
            Massive[i][j] = rand();
        }
    }
}

/**
* Конструктор копии
* @param matrixToCopy - Копируемая матрица
*/
Matrix::Matrix(Matrix& matrixToCopy)
{
    // инициализатор размера массива
    SizeN = matrixToCopy.SizeN;
    SizeM = matrixToCopy.SizeM;
    
    Massive = new int*[SizeN];// выделить место в памяти для матрицы

    for (int i = 0; i < SizeN; i++)
    {
        Massive[i] = new int[SizeM];
        for (int j = 0; j < SizeM; j++)
        {
            // заполняем матрицу значениями матрицы matrixToCopy
            Massive[i][j] = matrixToCopy[i][j];
        }
    }
}

/*
 * Десструктор класса Matrix
 */
Matrix::~Matrix()
{
    delete[]Massive;// освободить память, удалив матрицу
}

/**
* Перегруженный оператор взятия индекса
* @param Index - Индекс массива
*/
int*& Matrix::operator[](int Index)
{
    return Massive[Index];// возврат ссылки на элемент массива
}

/**
* Оператор присваивания
* @param copyMatrix - Присваиваемый массив
*/
Matrix& Matrix::operator=(Matrix& copyMatrix)
{
    if (this == &copyMatrix)// чтобы не выполнялось самоприсваивание
    {
        return *this;
    }

    delete []Massive;// освободить пространство

    // установить нужный размер матрицы
    SizeN = copyMatrix.SizeN;
    SizeM = copyMatrix.SizeM;
        
    Massive = new int*[SizeN];// выделить память под копируемый массив
    
    // скопировать массив
    for (int i = 0; i < SizeN; i++)
    {
        Massive[i] = new int[SizeM];
        for (int j = 0; j < SizeM; j++)
        {
            Massive[i][j] = copyMatrix[i][j];
        }
    }
    
    return *this;// разрешает множественное присваивание
}

/**
 * Перегруженный оператор вывода(вывод элементов массива на экран)
 * @param out - Вывод
 * @param m - Массив который будт выведен на экран
 */
std::ostream& operator<<(std::ostream& out, const Matrix& m)
{
    for (int i = 0; i < m.SizeN; i++)
    {
        for (int j = 0; j < m.SizeM; j++)
        {
            out << m.Massive[i][j] << " ";
        }
        out << "\n";// перенос маркера на новую строку
    }

    return out;
}

/**
* Перегруженный оператор ввода, для заполнения матрицы с клавиатуры
* @param input - Ввод
* @param m - Массив который будет заполнятся
*/
std::istream& operator>>(std::istream& input, Matrix& m)
{
    for (int i = 0; i < m.SizeN; i++)
    {
        for (int j = 0; j < m.SizeM; j++)
        {
            std::cout << "Input value number " << i << " " << j << " of matrix \n";
            input >> m.Massive[i][j];// заполняем матрицу
        }
    }
    return input;// позволяет множественный ввод
}
