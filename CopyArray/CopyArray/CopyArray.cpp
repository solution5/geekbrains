#include "iostream"
#include "Matrix.h"

int main(int argc, char* argv[])
{
    
    /**
    * Перегруженный оператор взятия индекса
    */
    Matrix Massive1(1, 2);
    std::cout << "Let's create matrix by [2, 2] size" << "\n";
    std::cin >> Massive1;
    std::cout << "Our matrix" << "\n";
    std::cout << Massive1;

    /*
     * Сделаем так, чтобы при копировании объекта данного
     * класса происходило глубокое копирование.
     */
    std::cout << "Let's call copy construct(Massive2) witch will copy Massive1 \n";
    Matrix Massive2(Massive1);// копируем массив
    std::cout << Massive2 << "\n";
    std::cout << "And make Massive3 witch will call assignment operator \n";
    Matrix Massive3;
    Massive3 = Massive2;
    std::cout << Massive3;
}
