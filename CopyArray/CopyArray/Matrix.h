#pragma once
#include "iostream"


class Matrix
{
    // перегруженный оператор вывода
    friend std::ostream& operator<<(std::ostream& out, const Matrix& m);
    // перегруженный оператор ввода
    friend std::istream& operator>>(std::istream& input, Matrix& m);

public:
    /*
    * В конструкторе инициализировать эти данные динамичеcки. 
    */
    int SizeN; // количество строчек
    int SizeM; // количество колон
    int** Massive; // создаим массив

    public:
    Matrix(); // конструктор по умолчанию
    Matrix(int N, int M); // конструктор с параметрами
    Matrix(Matrix& matrixToCopy); // конструктор копии
    ~Matrix(); // десструктор класса Matrix

    int* & operator[](int Index);
    Matrix& operator=(Matrix& copyMatrix);
};
