#include <iostream>
#include <vector>

/**
* Метод "произведение массивов"
* @param a - Коэффициенты первого полинома
* @param b - Коэффициенты второго полинома
* @param A - Коэффициент 1
*/
std::vector<int> ProductArrays(std::vector<int> &a, std::vector<int> &b, int A)
{
	A = 1;

	// Явно преобразуем размер вектора
	const int FirstSize = static_cast<int>(a.size());
	int FirstPolinom = 1;

	// Явно преобразуем размер вектора
	const int SecondSize = static_cast<int>(b.size());

	// Результирующий вектор
	std::vector<int> ResultVector(FirstSize + SecondSize - 1, 0);

	// Превратив первые набор коэффициентов в вектор 
	for(int i = 0; i < FirstSize; ++i)
	{
		a[i] *= FirstPolinom;
		FirstPolinom *= A;
	}

	// Перемножим вектора
	for(int i = 0; i < FirstSize; ++i)
	{
		for(int j = 0; j < FirstSize; ++j)
		{
			ResultVector[i+j] += a[i]*b[j];
		}
	}
	return ResultVector;
}

int main() {
	// Создадим коэффициенты двух полином a и b (как в примере из задания) 
	std::vector<int> a = {-1, 1};
	std::vector<int> b = {2, 1};

	std::vector<int> Result = ProductArrays(a, b, 1);

	for(int Value: Result)
	{
		std::cout << Value << ' ';
	}
	
	return 0;
}
